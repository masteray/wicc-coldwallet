var log = require('log4js').getLogger("dbIndex");
const { FILEPATH } = require('../utils/config');
const sqlite3 = require('sqlite3').verbose();

//查询所有数据
function querySqlAll(sql) {
    let db = new sqlite3.Database(FILEPATH);
    return new Promise((resolve, reject) => {
        try {
            //"select * from user"
            db.all(sql, function(err, row) {
                //console.log("=>", JSON.stringify(row));
                if (err) {
                    log.error(err);
                    reject(err)
                } else {
                    resolve(row)
                }
            })
        } catch (e) {
            log.error(e);
            reject(e)
        } finally {
            db.close()
        }
    })
}

//根据条件查询
function querySqlOne(sql, result) {
    let db = new sqlite3.Database(FILEPATH);
    return new Promise((resolve, reject) => {
        try {
            //"select * from user where username=?"
            db.each(sql, result, function(err, row) {
                if (err) {
                    log.error(err);
                    reject(err)
                } else {
                    resolve(row)
                }
            })
        } catch (e) {
            log.error(e);
            reject(e)
        } finally {
            db.close()
        }
    })
}



//插入数据到数据库
function insertSql(result) {
    let db = new sqlite3.Database(FILEPATH);
    return new Promise((resolve, reject) => {
        try {
            let a = result.length
            console.log("a=>", result)
            if (a == 0) {
                log.error({ code: 9000, msg: '传输给后台的参数有误' });
                reject({ code: 9000, msg: '传输给后台的参数有误' })
            }
            //插入助记词到mnemonic
            var add_mnemonic = db.prepare('insert into mnemonic (mnemonic,status) values (?,?)')
            add_mnemonic.run(result[0].mnemonic, 200, function(err, row) {
                if (err) {
                    log.error(err);
                    reject(err)
                } else {
                    querySqlOne('select * from mnemonic where mnemonic=?', result[0].mnemonic).then(resmnemonic => {
                        for (var i = 0; i < result.length; i++) {

                            var sql_add = db.prepare('insert into wallet (address,mnemonic_offset, mnemonic_id, pub_key,status) values (?,?,?,?,?)')

                            sql_add.run(result[i].address, result[i].indexaddress, resmnemonic.id, result[i].publickey, 200, function(err, row) {
                                a--
                                if (!err && a <= 0) {
                                    resolve({ code: 0 })
                                }
                                if (err) {
                                    log.error(err);
                                    reject(err)
                                }

                            })

                        }

                         
                    }).catch(err => {
                        log.error(err);
                        reject(err)
                    })

                }
            })


        } catch (e) {
            log.error(e);
            reject(e)
        } finally {
            //db.close()
        }
    })
}

//导入助记词及私钥
function insertImportKey(resultObj) {
    let db = new sqlite3.Database(FILEPATH);
    return new Promise((resolve, reject) => {
        try {
            db.all("select * from wallet where address=?", resultObj.address, function(err, row) {
                if (!err) {
                    if (row.length == 0) { //不存在该地址

                        if (resultObj.mnemonic) { //导入的是助记词
                            db.all("select * from mnemonic where mnemonic=?", resultObj.mnemonic, function(err, mnemo) {
                                if (!err) {

                                    if (mnemo.length == 0) { //不是公用的助记词
                                        var add_mnemonic = db.prepare('insert into mnemonic (mnemonic,status) values (?,?)')
                                        add_mnemonic.run(resultObj.mnemonic, 200, function(err, adrow) {
                                            if (err) {
                                                reject(err)
                                            } else {
                                                querySqlOne('select * from mnemonic where mnemonic=?', resultObj.mnemonic).then(resmnemonic => {

                                                    var sql_add = db.prepare('insert into wallet (address,mnemonic_offset, mnemonic_id, pub_key,status) values (?,?,?,?,?)')

                                                    sql_add.run(resultObj.address, resultObj.indexaddress, resmnemonic.id, resultObj.publickey, 200, function(err, row) {

                                                        if (!err) {
                                                            resolve({ code: 0 })
                                                        } else {
                                                            log.error(err);
                                                            reject(err)
                                                        }

                                                    }) 
                                                }).catch(err => {
                                                    log.error(err);
                                                    reject(err)
                                                })

                                            }
                                        })
                                    } else { //有公用的助记词
                                        querySqlOne('select * from mnemonic where mnemonic=?', resultObj.mnemonic).then(resmnemonic => {

                                            var sql_add = db.prepare('insert into wallet (address,mnemonic_offset, mnemonic_id, pub_key,status) values (?,?,?,?,?)')

                                            sql_add.run(resultObj.address, resultObj.indexaddress, resmnemonic.id, resultObj.publickey, 200, function(err, row) {

                                                if (!err) {
                                                    resolve({ code: 0 })
                                                } else {
                                                    log.error(err);
                                                    reject(err)
                                                }

                                            }) 
                                        }).catch(err => {
                                            log.error(err);
                                            reject(err)
                                        })
                                    }

                                } else {
                                    log.error(err);
                                }
                            })



                        } else if (resultObj.privatekey) { //导入的是私钥

                            var private_add = db.prepare('insert into wallet (address,priv_key,pub_key,status) values (?,?,?,?)')

                            private_add.run(resultObj.address, resultObj.privatekey, resultObj.publickey, 200, function(err, row) {

                                if (!err) {
                                    resolve({ code: 0 })
                                } else {
                                    log.error(err);
                                    reject(err)
                                }

                            }) 

                        } else {
                            log.error({ code: 9001, msg: '导入的参数有误' });
                            reject({ code: 9001, msg: '导入的参数有误' })
                        }
                    } else { //不需重复导入
                        resolve({ code: 100 })
                    }

                } else {
                    log.error(err);
                    reject(err)
                }

            })

        } catch (e) {
            log.error(e);
            reject(e)
        } finally {

        }
    })
}


module.exports = {
    querySqlAll,
    querySqlOne,
    insertSql,
    insertImportKey
}
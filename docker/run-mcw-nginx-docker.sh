WEB_DIR=~/MyColdWallet/wicc-coldwallet-webui/webui
cd $WEB_DIR && npm run build

docker run -d  \
	--name "wicc-mcw-webui" \
	-p 8080:80 \
	-v $(WEB_DIR)/src:/var/www/public \
	-v $(pwd)/nginx.conf:/etc/nginx/nginx.conf \
	--link wicc-mcw-nodejs:nodejs \
	nginx;
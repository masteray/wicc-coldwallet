import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
    routes: [{
            path: '/',
            redirect: '/exchange',
            component: resolve => require(['@/pages/index.vue'], resolve),
            children: [{
                    path: 'exchange',
                    name: 'exchange',
                    component: resolve => require(['@/pages/exchange/index.vue'], resolve)
                },
                {
                    path: 'createwallet',
                    name: 'createwallet',
                    component: resolve => require(['@/pages/wallet/createwallet.vue'], resolve)
                }
            ]
        }

    ]
})